package com.api.enity;

import com.api.enity.Fiscalia;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-06T09:27:23")
@StaticMetamodel(Departamento.class)
public class Departamento_ { 

    public static volatile SingularAttribute<Departamento, Integer> idDepartamento;
    public static volatile SingularAttribute<Departamento, Date> createdAt;
    public static volatile SingularAttribute<Departamento, String> nombre;
    public static volatile CollectionAttribute<Departamento, Fiscalia> fiscaliaCollection;

}