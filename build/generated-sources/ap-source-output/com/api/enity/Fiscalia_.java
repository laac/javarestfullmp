package com.api.enity;

import com.api.enity.Departamento;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-06T09:27:23")
@StaticMetamodel(Fiscalia.class)
public class Fiscalia_ { 

    public static volatile SingularAttribute<Fiscalia, Date> createdAt;
    public static volatile SingularAttribute<Fiscalia, Departamento> idDepartamento;
    public static volatile SingularAttribute<Fiscalia, Integer> idFiscalia;
    public static volatile SingularAttribute<Fiscalia, String> direccion;
    public static volatile SingularAttribute<Fiscalia, String> telefonos;
    public static volatile SingularAttribute<Fiscalia, String> nombre;

}