/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.enity.service;

import com.api.enity.Departamento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author 50230
 */
@Stateless
@Path("departamento")
public class DepartamentoFacadeREST extends AbstractFacadeDepartamento<Departamento> {

    @PersistenceContext(unitName = "PruebaApiMPPU")
    private EntityManager em;

    public DepartamentoFacadeREST() {
        super(Departamento.class);
    }


    @GET
    @Path("getDepartamentoByID/{id}")
    @Produces({ MediaType.APPLICATION_JSON})
    public Departamento find(@PathParam("id") Integer id) {
        return super.find(id);
    }
    
    @GET
    @Path("getDepartamentos")
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Departamento> getDepartamentos() {
        return super.getDepartamentos();
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Departamento> findAll() {
        return super.findAll();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
