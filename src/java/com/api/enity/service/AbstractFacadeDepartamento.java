/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.enity.service;

import com.api.enity.Departamento;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author 50230
 */
public abstract class AbstractFacadeDepartamento<T> {

    private Class<T> entityClass;

    public AbstractFacadeDepartamento(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public T find(Object id) {
        StoredProcedureQuery sp = 
          getEntityManager().createNamedStoredProcedureQuery("getDepartamentoByID").setParameter("id", id);
        if(!getEntityManager().createNamedStoredProcedureQuery("getDepartamentoByID").setParameter("id", id).getResultList().isEmpty())
            return (T) sp.getSingleResult();
        else 
            return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }
    
    public List<T> getDepartamentos() {
         StoredProcedureQuery sp = 
          getEntityManager().createNamedStoredProcedureQuery("getDepartamentos");
        return  sp.getResultList();
    }
    
}
