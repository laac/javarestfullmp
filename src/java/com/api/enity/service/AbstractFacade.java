/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.enity.service;

import com.api.enity.Fiscalia;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author 50230
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        StoredProcedureQuery sp = 
          getEntityManager().createNamedStoredProcedureQuery("createFiscalia")
                .setParameter("nombre", ((Fiscalia) entity).getNombre())
                .setParameter("direccion", ((Fiscalia) entity).getDireccion())
                .setParameter("id_departamento", ((Fiscalia) entity).getIdDepartamento().getIdDepartamento())
                .setParameter("telefonos", ((Fiscalia) entity).getTelefonos());
        sp.execute();
    }

    public void edit(T entity) {
        StoredProcedureQuery sp = 
          getEntityManager().createNamedStoredProcedureQuery("updateFiscalia")
                .setParameter("nombre", ((Fiscalia) entity).getNombre())
                .setParameter("direccion", ((Fiscalia) entity).getDireccion())
                .setParameter("id_departamento", ((Fiscalia) entity).getIdDepartamento().getIdDepartamento())
                .setParameter("telefonos", ((Fiscalia) entity).getTelefonos())
                .setParameter("id", ((Fiscalia) entity).getIdFiscalia());
        sp.execute();
    }

    public void remove(T entity) {
       StoredProcedureQuery sp = 
          getEntityManager().createNamedStoredProcedureQuery("deleteFiscalia")
                .setParameter("id", ((Fiscalia) entity).getIdFiscalia());
        sp.execute();
    }

    public T find(Object id) {
         StoredProcedureQuery sp = 
          getEntityManager().createNamedStoredProcedureQuery("getFiscaliaByID").setParameter("id", id);
        if(!getEntityManager().createNamedStoredProcedureQuery("getFiscaliaByID").setParameter("id", id).getResultList().isEmpty())
            return (T) sp.getSingleResult();
        else 
            return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
       StoredProcedureQuery sp = 
          getEntityManager().createNamedStoredProcedureQuery("getFiscalias");
        return  sp.getResultList();
    }
    
}
