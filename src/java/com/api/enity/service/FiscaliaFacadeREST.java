/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.enity.service;

import com.api.enity.Fiscalia;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author 50230
 */
@Stateless
@Path("fiscalia")
public class FiscaliaFacadeREST extends AbstractFacade<Fiscalia> {

    @PersistenceContext(unitName = "PruebaApiMPPU")
    private EntityManager em;

    public FiscaliaFacadeREST() {
        super(Fiscalia.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_JSON})
    public void create(Fiscalia entity) {
        super.create(entity);
    }

    @POST
    @Override
    @Path("edit")
    @Consumes({MediaType.APPLICATION_JSON})
    public void edit(Fiscalia entity) {
        super.edit(entity);
    }

    @POST
    @Path("remove")
    @Override
    public void remove(Fiscalia entity) {
        super.remove(entity);
    }

    @GET
    @Path("getfiscaliaByID/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Fiscalia find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Fiscalia> findAll() {
        return super.findAll();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
