/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.enity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 50230
 */
@Entity
@Table(name = "fiscalia")
@XmlRootElement
@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(
	name = "getFiscalias", 
	procedureName = "getFiscalias", 
        resultClasses = { Fiscalia.class }, 
	parameters = { 
	}
    ),
    @NamedStoredProcedureQuery(
	name = "getFiscaliaByID", 
	procedureName = "getFiscaliaByID", 
        resultClasses = { Fiscalia.class }, 
	parameters = { 
          @StoredProcedureParameter(name = "id", type = Integer.class,  mode = ParameterMode.IN) 
	}
    ),
    @NamedStoredProcedureQuery(
	name = "createFiscalia", 
	procedureName = "insertFiscalia", 
        resultClasses = { Fiscalia.class }, 
	parameters = { 
          @StoredProcedureParameter(name = "nombre", type = String.class,  mode = ParameterMode.IN),
          @StoredProcedureParameter(name = "direccion", type = String.class,  mode = ParameterMode.IN),
          @StoredProcedureParameter(name = "telefonos", type = String.class,  mode = ParameterMode.IN), 
          @StoredProcedureParameter(name = "id_departamento", type = Integer.class,  mode = ParameterMode.IN) 
               
	}
    ),
    @NamedStoredProcedureQuery(
	name = "updateFiscalia", 
	procedureName = "updateFiscalia", 
        resultClasses = { Fiscalia.class }, 
	parameters = { 
          @StoredProcedureParameter(name = "nombre", type = String.class,  mode = ParameterMode.IN),
          @StoredProcedureParameter(name = "direccion", type = String.class,  mode = ParameterMode.IN),
          @StoredProcedureParameter(name = "telefonos", type = String.class,  mode = ParameterMode.IN), 
          @StoredProcedureParameter(name = "id_departamento", type = Integer.class,  mode = ParameterMode.IN),
          @StoredProcedureParameter(name = "id", type = Integer.class,  mode = ParameterMode.IN) 
               
	}
    ),
    @NamedStoredProcedureQuery(
	name = "deleteFiscalia", 
	procedureName = "deleteFiscalia", 
        resultClasses = { Fiscalia.class }, 
	parameters = { 
          @StoredProcedureParameter(name = "id", type = Integer.class,  mode = ParameterMode.IN) 
	}
    ),
})
@NamedQueries({
    @NamedQuery(name = "Fiscalia.findAll", query = "SELECT f FROM Fiscalia f")
    , @NamedQuery(name = "Fiscalia.findByIdFiscalia", query = "SELECT f FROM Fiscalia f WHERE f.idFiscalia = :idFiscalia")
    , @NamedQuery(name = "Fiscalia.findByNombre", query = "SELECT f FROM Fiscalia f WHERE f.nombre = :nombre")
    , @NamedQuery(name = "Fiscalia.findByDireccion", query = "SELECT f FROM Fiscalia f WHERE f.direccion = :direccion")
    , @NamedQuery(name = "Fiscalia.findByCreatedAt", query = "SELECT f FROM Fiscalia f WHERE f.createdAt = :createdAt")
    , @NamedQuery(name = "Fiscalia.findByTelefonos", query = "SELECT f FROM Fiscalia f WHERE f.telefonos = :telefonos")})
public class Fiscalia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue (strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_fiscalia")
    private Integer idFiscalia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 350)
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Size(max = 100)
    @Column(name = "telefonos")
    private String telefonos;
    @JoinColumn(name = "id_departamento", referencedColumnName = "id_departamento")
    @ManyToOne(optional = false)
    private Departamento idDepartamento;

    public Fiscalia() {
    }

    public Fiscalia(Integer idFiscalia) {
        this.idFiscalia = idFiscalia;
    }

    public Fiscalia(Integer idFiscalia, String nombre, String direccion) {
        this.idFiscalia = idFiscalia;
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public Integer getIdFiscalia() {
        return idFiscalia;
    }

    public void setIdFiscalia(Integer idFiscalia) {
        this.idFiscalia = idFiscalia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Departamento idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFiscalia != null ? idFiscalia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fiscalia)) {
            return false;
        }
        Fiscalia other = (Fiscalia) object;
        if ((this.idFiscalia == null && other.idFiscalia != null) || (this.idFiscalia != null && !this.idFiscalia.equals(other.idFiscalia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.api.enity.Fiscalia[ idFiscalia=" + idFiscalia + " ]";
    }
    
}
